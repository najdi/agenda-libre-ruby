json.array!(@users) do |user|
  json.extract! user, :login, :email, :lastname, :firstname
  json.url user_url(user, format: :json)
end
