json.array!(@events) do |event|
  json.extract! event, :id, :title, :description, :start_time, :end_time,
                :place_name, :address, :city, :region_id, :locality, :url,
                :contact, :submitter, :moderated, :tags, :secret,
                :decision_time, :submission_time, :moderator_mail_id,
                :submitter_mail_id
  json.url event_url(event, format: :json)
end
