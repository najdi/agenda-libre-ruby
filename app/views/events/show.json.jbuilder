json.extract! @event, :id, :title, :description, :start_time, :end_time,
              :place_name, :address, :city, :region, :locality, :url, :contact,
              :contact, :submitter, :moderated, :tags, :secret, :decision_time,
              :submission_time, :moderator_mail_id, :submitter_mail_id
