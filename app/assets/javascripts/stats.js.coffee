$(document).ready ->
  # Sparline generation along the data tables
  $('table.list.dates tbody tr').each ->
    vals = $(this).find('td.quantity').map ->
      val = $(this).find('a').html().replace(' ', '').trim()
      if (val && val != '')
        return parseInt val
      else
        return 0

    $(this).find('.sparkline').sparkline vals, width: '5em'

  $('table.list.dates tfoot').each ->
    vals = $(this).find('th.quantity').map ->
      return parseInt $(this).html().replace(' ', '')

    $(this).find('.sparkline').sparkline vals,
      type: 'bar'
      height: '3em'
      barWidth: '100%'
      barColor: '#9CC5EE'
      barSpacing: 2
