# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# compiled file.
#
# Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
# about supported directives.
#
#= require jquery
#= require jquery_ujs
#= require jquery.sparkline
#= require jquery.turbolinks
#= require turbolinks
#= require tinymce-jquery
#= require modernizr
#= require webshims/polyfiller
#= require select2
#= require select2_locale_fr
#= require leaflet
#= require leaflet.markercluster
#= require_tree .

# Setup polyfills, so that older browsers can also take advantage of html5!
$.webshims.setOptions 'basePath', '/webshims/1.15.10/shims/'
$.webshims.setOptions 'forms-ext',
  'widgets':
    'startView': 2,
    'stepfactor': 10,
    'classes': 'show-yearbtns hide-btnrow show-uparrow'
$.webshims.polyfill 'forms forms-ext'

Turbolinks.enableProgressBar()

$(document).on 'page:fetch submit', ->
  $('em#loading').fadeIn('slow')
$(document).on 'page:change ready', ->
  $('em#loading').fadeOut('slow')

$(document).on 'page:load', ->
  # Reload polyfill when turbolinks loads a new page
  $(this).updatePolyfill()

$(document).ready ->
  # Hides the chrome broken image when image is absent
  if !Modernizr.testAllProps('forceBrokenImageIcon')
    $('img.favicon').one 'error', ->
      $(this).css visibility: 'hidden'

  # $('select').select2()
