# Setting up OpenStreeMap from a generic #map element
$(document).ready ->
  $('#map.events').each ->
    map = L.map 'map'

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
    ).addTo map

    url = $(this).data 'url'
    if location.search && url.indexOf('?') >= 0
      url += '&' + location.search.substr 1
    else
      url += location.search

    $.getJSON url, (json) ->
      layer = L.geoJson json,
        onEachFeature: (feature, layer) ->
          # Does this feature have a property named popupContent?
          if (feature.properties && feature.properties.popupContent)
            layer.bindPopup feature.properties.popupContent

      map.addLayer L.markerClusterGroup( maxClusterRadius: 30 ).addLayer layer

      if (layer.getBounds()._northEast && layer.getBounds()._southWest)
        # Automatic focus to all displayed events
        map.fitBounds layer.getBounds()
      else
        $('#map.events').remove()

  $('#map.event').each ->
    coord = [$(this).data('latitude'), $(this).data('longitude')]

    map = L.map('map').setView [coord[0], coord[1]], 16

    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a>'
    ).addTo map

    url = $(this).data 'url'
    if location.search && url.indexOf('?') >= 0
      url += '&' + location.search.substr 1
    else
      url += location.search

    $.getJSON url, (json) ->
      layer = L.geoJson json,
        onEachFeature: (feature, layer) ->
          # Does this feature have a property named popupContent?
          if (feature.properties && feature.properties.popupContent)
            layer.bindPopup(feature.properties.popupContent)

      map.addLayer L.markerClusterGroup( maxClusterRadius: 30 ).addLayer layer

    marker = L.marker([coord[0], coord[1]]).addTo map
