# Helper for the orga views
module OrgasHelper
  def set_orga_meta
    set_meta_tags \
      description: @orga.url,
      DC: { title: @orga.name },
      geo: {
        region: @orga.region,
        placename: @orga.city,
        position: "#{@orga.city.try :latitude}; #{@orga.city.try :longitude}"
      }
  end
end
