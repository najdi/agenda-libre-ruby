ActiveAdmin.register Orga do
  permit_params :kind_id, :name, :url, :city, :department, :region_id

  index do
    column :kind
    column :name
    column :url
    column :city
    column :department
    column :region
    actions
  end

  show do |l|
    attributes_table do
      row :kind
      row :name
      row :url do
        link_to l.url, l.url
      end
      row :city
      row :department
      row :region
    end
    active_admin_comments
  end

  form do |f|
    f.inputs do
      f.input :kind
      f.input :name
      f.input :url
      f.input :city
      f.input :department
      f.input :region
    end
    f.actions
  end
end
