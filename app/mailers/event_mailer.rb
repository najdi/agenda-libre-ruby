# Sending mails related to events life cycle
class EventMailer < ApplicationMailer
  helper :events

  def create(event)
    @event = event

    mail 'Message-ID' =>
      "<event-#{event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         to: event.submitter,
         subject: "#{t 'mail_prefix'}#{t 'event_mailer.create.subject',
                                         subject: event.title}"
  end

  def accept(event)
    @event = event
    @current_user = User.find_by id: event.paper_trail_originator

    mail 'In-Reply-To' =>
      "<event-#{event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         to: event.submitter,
         subject: "#{t 'mail_prefix'}#{t 'event_mailer.accept.subject',
                                         subject: event.title}"
  end

  def destroy(event)
    @event = event
    @current_user = User.find_by id: event.paper_trail_originator

    mail 'In-Reply-To' =>
      "<event-#{event.id}@#{ActionMailer::Base.default_url_options[:host]}>",
         to: event.submitter,
         subject: "#{t 'mail_prefix'}#{t 'event_mailer.destroy.subject',
                                         subject: event.title}"
  end
end
