# Geocoding
#
# Access to OSM controls
class MapsController < ApplicationController
  has_scope :region, :locality, :tag, :daylimit
  has_scope :future, type: :boolean, default: true
  has_scope :period, type: :hash, using: [:year, :week]

  def index
    respond_to do |format|
      format.html { render layout: 'iframe' if params[:iframe] }
      format.json { render json: apply_scopes(Event.moderated.geo) }
    end
  end
end
