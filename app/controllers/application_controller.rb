# The top level controller, where can be centralised almost everything
class ApplicationController < ActionController::Base
  before_action :set_locale
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  def set_locale
    I18n.locale =
      http_accept_language.compatible_language_from I18n.available_locales
  end

  protected

  # Useful to manage absolute url in mails
  def set_mailer_host
    ActionMailer::Base.default_url_options[:host] = request.host_with_port
  end
end
