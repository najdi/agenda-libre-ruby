# Manages data related to events' moderation
class Note < ActiveRecord::Base
  belongs_to :event
  belongs_to :author, class_name: User

  validates :contents, presence: true

  # Setup the magic time stamp so it uses the "date" column
  def timestamp_attributes_for_create
    super << :date
  end
end
