# Groups related to this agenda
class Orga < ActiveRecord::Base
  has_paper_trail

  belongs_to :region
  belongs_to :kind

  validates :kind, presence: true
  validates :name, presence: true
  validates :region, presence: true
  validates :url, format: %r{\Ahttps?:\/\/.*\..*\z}
  validates :feed, allow_blank: true, format: %r{\Ahttps?:\/\/.*\..*\z}
  validates :contact, presence: true, email: true
  validates :submitter, allow_blank: true, email: true

  scope :moderated, -> { where moderated: true }
  scope :unmoderated, -> { where moderated: false }

  before_validation do
    self.secret ||= SecureRandom.urlsafe_base64(32)[0...32]
    self.submission_time ||= Time.zone.now

    # Populate submitter using contact info if absent
    self.submitter = contact if submitter.blank?
  end

  after_create do
    send_secret
  end

  after_update do
    send_secret if secret_changed?

    if moderated_changed?
      OrgaMailer.accept(self).deliver_now!
    else
      OrgaMailer.update(self).deliver_now!
    end
  end

  before_destroy do
    OrgaMailer.destroy(self).deliver_now!
  end

  def send_secret
    OrgaMailer.create(self).deliver_now!
  end

  def name_as_tag
    name.gsub(/[\s\*']/, '-').delete ':'
  end
end
