# This class is linked to events and organisations
#
# It is mainly used to manage coordinates
#
class City < ActiveRecord::Base
  has_many :events, foreign_key: :city, primary_key: :name
  has_many :orgas, foreign_key: :city, primary_key: :name
end
