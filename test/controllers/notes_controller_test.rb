require 'test_helper'

# Test the notes life cycle
class NotesControllerTest < ActionController::TestCase
  include Devise::TestHelpers

  setup do
    @note = notes(:one)

    sign_in users(:one)
  end

  test 'should get new' do
    get :new, moderation_id: @note.event.id
    assert_response :success
  end

  test 'should create note' do
    assert_difference 'Note.count' do
      post :create, moderation_id: @note.event.id, note: {
        contents: @note.contents
      }
    end

    assert_redirected_to moderations_path
  end

  test 'should send mail' do
    assert_difference 'Note.count' do
      post :create, moderation_id: @note.event.id, envoiParMail: 'oui', note: {
        contents: @note.contents
      }
    end

    assert ActionMailer::Base.deliveries.present?

    assert_redirected_to moderations_path
  end

  test 'should not create note' do
    assert_no_difference 'Note.count' do
      post :create, moderation_id: @note.event.id, note: {
        nothing: 'almost'
      }
    end
  end
end
