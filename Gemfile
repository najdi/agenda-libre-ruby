source 'https://rubygems.org'

# The central piece of this application: the month calendar view
gem 'simple_calendar'

gem 'rails'
gem 'has_scope'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'

gem 'haml-rails'

gem 'devise'
gem 'devise-i18n'
gem 'rails-i18n'
# Store some specific content, like application name and static pages contents
gem 'i18n-active_record',
    github: 'svenfuchs/i18n-active_record',
    require: 'i18n/active_record'
gem 'http_accept_language'
# Remove leading and trailing white spaces
gem 'strip_attributes'
# Track changes to models' data
gem 'paper_trail', '~> 4.0.0.rc'

# Validate mails submitted
gem 'email_validator'
# Email address obfuscation
gem 'actionview-encoded_mail_to'
# To display a patched diff for event descriptions
gem 'differ'

# A generic library to administrate the tool
gem 'activeadmin', '~> 1.0.0.pre1'
# A nicer markdown editor in active admin
gem 'activeadmin_pagedown'

gem 'jquery-sparkline-rails', github: 'cubus/jquery-sparkline-rails'

# Markdown display
gem 'redcarpet'

# Carte openstreetmap
gem 'leaflet-rails'
gem 'leaflet-markercluster-rails'

gem 'geocoder'

# Tiny MCE integration
gem 'tinymce-rails'
gem 'tinymce-rails-langs'

# SEO optimisations
gem 'meta-tags'

gem 'twitter'

group :development do
  gem 'sqlite3'
  gem 'webrick'
  gem 'quiet_assets'
  gem 'guard-migrate', require: false
  gem 'guard-bundler', require: false
  gem 'guard-rubocop', require: false
  gem 'guard-minitest', require: false
  gem 'guard-livereload', require: false
  gem 'rack-livereload'
  gem 'haml-lint'
  # Security checks
  gem 'brakeman', require: false
  gem 'guard-brakeman', require: false
end

group :test do
  gem 'simplecov'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger
  # console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the
  # background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :development, :test, :assets do
  # Use SASS for stylesheets, this version is required to compile active_admin
  # assets, TODO
  gem 'sass-rails'
  # Use CoffeeScript for .coffee assets and views
  gem 'coffee-rails'

  # Use jquery as the JavaScript library
  gem 'jquery-rails'
  # Turbolinks makes following links in your web application
  # faster. Read more: https://github.com/rails/turbolinks
  gem 'turbolinks'
  gem 'jquery-turbolinks'
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', '~> 0.4.0', group: :doc

  gem 'compass-rails', '~> 2.0.4'
  gem 'modernizr-rails'

  # Patch older browsers so they do understand html5
  gem 'webshims-rails'

  # Improve the html select element
  gem 'select2-rails', '< 4'

  # A superb font to use as icons
  gem 'font-awesome-sass'
end

group :assets do
  # Use Uglifier as compressor for JavaScript assets
  gem 'uglifier'
  # See https://github.com/sstephenson/execjs#readme for more supported runtimes
  gem 'therubyracer', platforms: :ruby
end

group :production do
  # Use mysql as the database for Active Record
  gem 'mysql2', '~> 0.3.18'
end
