# Setting up organisations' kinds
class CreateKinds < ActiveRecord::Migration
  def change
    create_table :kinds do |t|
      t.string :name, unique: true, null: false
      t.string :icon

      t.timestamps null: false
    end

    # Create the relevant organisation kinds
    Kind.create name: 'association', icon: 'sitemap'
    Kind.create name: 'enterprise', icon: 'building'
    Kind.create name: 'lug', icon: 'support'
    Kind.create name: 'provider', icon: 'tty'
    Kind.create name: 'institution', icon: 'institution'
  end
end
