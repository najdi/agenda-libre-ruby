# Generalisation of the LUG concept to one of event's organiser
class RenameTableLug < ActiveRecord::Migration
  def change
    rename_table :lugs, :orgas
  end
end
